organization := "Project Weberia"

name := "weberia-scalia"

version := "1.0.0"

scalaVersion := "2.11.7"

resolvers ++= Seq(
  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases",
  "apache-repo-releases" at "http://repository.apache.org/content/repositories/releases/",
  "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"
)

libraryDependencies ++= {

  val (akkaV, streamHttp) = ("2.3.12", "1.0")

  // remember, %% means scala-major-version, so %% "bigdata" below will 
  // result error since there is no bigdata_2.11 version. use % instead
  // see: http://www.scala-sbt.org/0.13/tutorial/Library-Dependencies.html

  Seq(
    "org.scala-lang.modules"  %%  "scala-parser-combinators"    % "1.0.4",
    "com.tumblr"              %%  "colossus"                    % "0.6.4",
    "com.typesafe.akka"       %%  "akka-stream-experimental"    % streamHttp,
    "com.typesafe.akka"       %%  "akka-http-core-experimental" % streamHttp,
    "com.typesafe.akka"       %%  "akka-http-experimental"      % streamHttp,
    //"org.specs2"              %%  "specs2-core"               % "3.6.2" % "test"
    "org.scalactic"           %%  "scalactic"                 % "2.2.4",
    "org.scalatest"           %%  "scalatest"                 % "2.2.4" % "test"
  )

}

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")
